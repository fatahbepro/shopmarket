const routes = [
    {
        path: '/',
        redirect: '/user/orderList'
    },
    {
        path: '/user/orderList',
        name: 'orderList',
        component: () => import('./order-list/order-list.vue')
    },
    {
        path: '/order/details/:id',
        name: 'orderDetails',
        component: () => import('./order-details/order-details.vue')
    },
    {
        path: '/order/cancel/:id',
        name: 'orderCancel',
        component: () => import('./cancel-order/order-cancel.vue')
    },
    {
        path: '/order/claim/:id',
        name: 'orderClaim',
        component: () => import('./claim-order/order-claim.vue')
    },
    {
        path: '/order/survey/:id',
        name: 'orderClaim',
        component: () => import('./claim-order/order-claim.vue')
    }
]

export default routes