/* eslint-disable no-unused-vars */

import service from "@/order/service";

const order = {
  namespaced: true,

  state: {
    orderList: [
      {
        Row: 1,
        OrderId: 1,
        DeliveryRequestedDateDesc: "جمعه 20 مهر – 14 تا 17",
        OrderStatusId: "تحویل شده",
        TotalPayment: 280000,
        IsSurveyed: "شده",
        IsClaimed: 0,
        IsCancelable: 1,
      },
      {
        Row: 2,
        OrderId: 2,
        DeliveryRequestedDateDesc: "شنبه 18 آبان – 11 تا 14",
        OrderStatusId: "لغو شده",
        TotalPayment: 100000,
        IsSurveyed: "-",
        IsClaimed: 0,
        IsCancelable: 0,
      },
      {
        Row: 3,
        OrderId: 3,
        DeliveryRequestedDateDesc: "جمعه 20 مهر – 14 تا 17",
        OrderStatusId: "در دست اقدام",
        TotalPayment: 50000,
        IsSurveyed: "نشده",
        IsClaimed: 1,
        IsCancelable: 1,
      },
      {
        Row: 4,
        OrderId: 4,
        DeliveryRequestedDateDesc: "سه شنبه 20 اسفند – 8 تا 11",
        OrderStatusId: "در دست اقدام",
        TotalPayment: 28000,
        IsSurveyed: "شده",
        IsClaimed: 0,
        IsCancelable: 1,
      },
    ],
    order: {
      DeliveryRequestedDateDesc: "شنبه 18 آبان – ساعت 11:00 تا 14:00",
      DeliveryAddress: "بلوار فرهنگ بلوار سید رضی بین سید رضی 14 و 16 پلاک 12",
      TotalGoodCount: "6 عدد",
      TotalPrice: "3450000",
      TransmitCost: "رایگان",
      CreditAmountUsed: "2100000",
      PaiedAmount: "1350000",
      Deliverycode: "43244234-5558",
      items: [
        {
          Row: "1",
          GoodName: "تخم مرغ تلاونگ",
          GoodAmount: "3",
          JcoPrice: "5000",
          SumGoodJCoPrice: "15000",
          img:'https://api.snapp.market/media/cache/product-variation_image_thumbnail/uploads/images/vendors/users/app/20210810-310456-1.jpg'
        },
        {
          Row: "2",
          GoodName: "تون ماهی galaxy",
          GoodAmount: "1",
          JcoPrice: "45000",
          SumGoodJCoPrice: "45000",
          img:'https://api.snapp.market/media/cache/product-variation_image_thumbnail/uploads/images/vendors/users/app/20210818-177823-1.jpg'
        },
        {
          Row: "3",
          GoodName: "برنج تایلندی جی تی سی ۱۰ کیلوگرمی",
          GoodAmount: "3",
          JcoPrice: "5000",
          SumGoodJCoPrice: "15000",
          img:'https://api.snapp.market/media/cache/product-variation_image_thumbnail/uploads/images/vendors/users/app/20210509-380065-1.jpg'
        },
      ],
    },
  },

  mutations: {},

  actions: {
    async getOrderList(context) {
      return context.state.orderList;
      // return await service.getOrderList();
    },
    async getOrderDetails(context, orderId) {
      return context.state.order;
      // return await service.getOrderDetails(orderId);
    },
    async surveyOrder(context, orderId) {
      return "success";
      // return await service.surveyOrder(orderId);
    },
        async cancelOrder(context, params) {
            return 'success';
            // return await service.cancelOrder(orderId);
        },
        async claimOrder(context, params) {
            return 'success';
            // return await service.claimOrder(params);
        },
  },
  getters: {},
};

export default order;
