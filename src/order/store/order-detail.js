/* eslint-disable no-unused-vars */

import service from "@/order/service";

const details = {
  namespaced: true,

  state: {
    orderDetails: [
      {
        Row: 1,
        GoodName: "بیسکوییت سبوس دار",
        GoodAmount: 1,
        JcoPrice: 50000,
        SumGoodJCoPrice: 50000,
      },
      {
        Row: 2,
        GoodName: "بیسکوییت سبوس دار",
        GoodAmount: 1,
        JcoPrice: 50000,
        SumGoodJCoPrice: 50000,
      },
      {
        Row: 3,
        GoodName: "بیسکوییت سبوس دار",
        GoodAmount: 1,
        JcoPrice: 50000,
        SumGoodJCoPrice: 50000,
      },
      {
        Row: 4,
        GoodName: "بیسکوییت سبوس دار",
        GoodAmount: 2,
        JcoPrice: 50000,
        SumGoodJCoPrice: 100000,
      },
    ],
  },

  mutations: {},

  actions: {
    async getOrderDetails(context) {
      console.log("-------------------");
      return context.state.orderDetails;
      // return await service.getOrderDetails();
    },
  },

  getters: {},
};

export default details;
