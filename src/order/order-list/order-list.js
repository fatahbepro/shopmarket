import {isMobileDevice} from "@/common/utils/isMobileDevice";
import {createNamespacedHelpers} from "vuex";
import dataTable from "@/common/components/table/data-table.vue";
import cancelOrderModal from "@/order/order-list/components/cancelOrderModal";
import surveyModal from "@/order/order-list/components/surveyModal";
import claimModal from "@/order/order-list/components/claimModal";

const {mapActions} = createNamespacedHelpers("orderStore");
export default {
    name: "OrderList",
    data() {
        return {
            isMobile: false,
            orders: [],
            headers: [
                "ردیف",
                "زمان تحویل",
                "وضعیت",
                "مبلغ کل",
                "جزئیات",
                "نظر سنجی",
                "سایر امکانات",
            ],
        };
    },
    components: {
        dataTable,
        cancelOrderModal,
        surveyModal,
        claimModal
    },
    created() {
        this.showOrdersList();
    },
    methods: {
        ...mapActions(["getOrderList"]),
        showOrdersList() {
            this.isMobile = isMobileDevice();
            //TODO: getOrderList from serve
            this.getOrderList()
                .then((res) => {
                    this.orders = res;
                })
                .catch((error) => {
                    throw new Error(error.message);
                });
        },
        showDetails(orderId) {
            this.$router.push("/order/details/" + orderId);
        },
        doSurvey(orderId) {
            // this.$router.push("/order/survey/" + orderId);
            //     this.modal = {
            //         title: 'نظرسنجی سفارش ردیف 3',
            //         btnTitle: ['انصراف', 'ثبت نظر'],
            //         id: 'order-modal',
            //         callMethods: ['cancelSurvey', 'doSurvey']
            //
            //     }
            //     bsModal.toggle();
            console.log(orderId)
            // this.modalToggle()
        },
        cancelOrder(orderId, isCancelable) {
            console.log(orderId)
            console.log(isCancelable)

            // if (isCancelable == 1) {
            // } else {
            // }
        },
        claimOrder(orderId) {
            console.log(orderId)

        },
    },
};
