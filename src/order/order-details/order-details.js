import dataTable from "@/common/components/table/data-table.vue";
import { createNamespacedHelpers } from "vuex";
const { mapActions } = createNamespacedHelpers("orderStore");
export default {
  name: "orderDetails",
  components: {
    dataTable,
  },
  data() {
    return {
      order: [],
      headers: ["ردیف","تصویر", "نام کالا", "تعداد", "قیمت (تومان)", "مجموع(تومان)"],
    };
  },
  created() {
    this.showOrdersList();
  },
  methods: {
    ...mapActions(["getOrderDetails"]),
    showOrdersList() {
      //TODO: getOrderDetails from serve
      this.getOrderDetails(20)
        .then((res) => {
          this.order = res;
        })
        .catch((error) => {
          throw new Error(error.message);
        });
    },
    backToOrderList(){
      this.$router.push('/user/orderlist')
    }
  },
};
