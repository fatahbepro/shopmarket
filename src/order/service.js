import axios from "axios";

const UserToken = null;
export default {
    getOrderList() {
        return axios({
            method: 'GET',
            url: 'https://Jco.market/user/orderlist',
            headers: {
                authorization: UserToken
            }

        });
    },
    getOrderDetails(params) {
        return axios({
            method: 'POST',
            url: 'https://Jco.market/order/orderDetails',
            data: params,
            headers: {
                authorization: UserToken
            }
        });
    },
    surveyOrder(params) {
        return axios({
            method: 'POST',
            url: 'https://Jco.market/order/surveyOrder',
            data: params,
            headers: {
                authorization: UserToken
            }
        });
    },
    cancelOrder(params) {
        return axios({
            method: 'POST',
            url: 'https://Jco.market/order/cancelOrder',
            data: params,
            headers: {
                authorization: UserToken
            }
        });
    },
    claimOrder(params) {
        return axios({
            method: 'POST',
            url: 'https://Jco.market/order/claimOrder',
            data: params,
            headers: {
                authorization: UserToken
            }
        });
    }
}
