import "@/common/assets/styles/main.scss";
import {createApp} from "vue";
import App from "./App.vue";
import router from "@/common/router/index";
import store from "@/common/store/index";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";
import Toast from "vue-toastification";

import "vue-toastification/dist/index.css";


createApp(App).use(router).use(store).use(Toast).mount("#app");
