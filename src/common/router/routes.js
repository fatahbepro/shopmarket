import orderRoutes from '@/order/routes.js'


const routes = [
    ...orderRoutes,

]

export default routes
