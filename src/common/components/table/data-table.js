export default {
    name: "DataTable",
    data() {
        return {
            orderStatus: {
                "تحویل شده": "text-success",
                "لغو شده": "text-danger",
                "در دست اقدام": "text-info",
                شده: "text-secondary",
                نشده: "text-primary",
            },
            cancelModalAttr: {
                'data-bs-toggle': "modal",
                'data-bs-target': "#cancelModal"
            },
            surveyModalAttr: {
                'data-bs-toggle': "modal",
                'data-bs-target': "#surveyModal"
            }, claimModalAttr: {
                'data-bs-toggle': "modal",
                'data-bs-target': "#claimModal"
            },

        };
    },
    props: {
        headers: {
            type: Array,
            required: true,
        },
        items: {
            type: Array,
            required: true,
        },
        search: {
            type: String,
        },

        itemsPerPage: {
            type: Number,
            required: false,
        },

        searchDisabled: {
            type: Boolean,
        },
    },
    methods: {
        showDetails(orderId) {
            this.$emit("showDetails", orderId);
        },
        checkStatus(orderStatusId) {
            return this.orderStatus[orderStatusId];
        },
        cancelOrder(orderId, isCancelable, elm) {
            this.$emit("cancelOrder", orderId, isCancelable);
            this.activeRow(elm.target.parentElement.parentElement)

        },
        claimOrder(orderId, isClaimed, elm) {
            if (isClaimed == 0) {
                this.$emit("claimOrder", orderId);
                this.activeRow(elm.target.parentElement.parentElement)

            } else {
                return;
            }
        },
        doSurvey(orderId, IsSurveyed, elm) {
            if (IsSurveyed == 'نشده') {
                this.$emit("doSurvey", orderId);
                this.activeRow(elm.target.parentElement)
            } else {
                return;
            }
        },
        activeRow(currentRow) {
            const bodyRows = currentRow.parentElement.children;
            for (let i = 0; bodyRows.length > i; i++) {
                bodyRows[i].classList.remove('table-active')
            }
            currentRow.classList.add('table-active');

        },
        checkAttr(status, attrObj, conditional) {

            return status == conditional ? attrObj : null;

        }

    },

};
