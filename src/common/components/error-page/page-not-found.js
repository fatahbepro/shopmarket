export default {
    name: 'Page404',
    methods: {
        backPage() {
            this.$router.go(-2);
        },
        backHome() {
            this.$router.push({ path: '/' });
        }
    }
};
