import {createStore} from 'vuex'
import modules from "@/common/store/store-modules.js";

const store = createStore({
  modules
})
export default store
