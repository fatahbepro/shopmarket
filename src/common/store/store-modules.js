import orderStore from "@/order/store/order-store.js";

const storeModules = {
  orderStore,
};

export default storeModules;
